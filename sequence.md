```plantuml
@startuml
skinparam componentStyle rectangle

loop Once Per Day
    CMSTGtinLookup -> GtinQueryCmstAdapter : GET Product reference-file
    GtinQueryCmstAdapter -> CMSTGtinLookup : Product Reference (JSON)
end
loop Once Per Day
    GtinQueryCmstAdapter -> ProductMediator : GET /product once per day
    ProductMediator -> GtinQueryCmstAdapter : Bundle<Product>
end

@enduml
```

