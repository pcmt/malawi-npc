```plantuml
@startuml
skinparam componentStyle rectangle
'skinparam ComponentBackgroundColor #green
skinparam linetype ortho

frame "National Product Catalog" {
    component pcmt [
        PCMT

        Continual enrichment of national product master data 
        and sharing to downstream systems
    ]

    component pcmtFhirApi [
        PCMT FHIR API

        Provide NPC master data in FHIR format.
    ]

    [pcmt] <-- [pcmtFhirApi] : PCMT Product and ProductModel
}

frame "Interoperability Layer" {
    component productMediator [
        Product Mediator

        Single point of passthrough for Interoperability Layer 
        mediators for credentials and audit purpose.
    ]
    
    frame "CMST Adapters" {
      component gtinCmstAdaptorMediator [
          GTIN CMST Adaptor Mediator

          Adapts product master data to flat reference file of key (gtin) to values 
          (product codes, manufacturer, etc).  Does this on a set schedule, storing 
          the single reference file for retrieval by the CMST system via HTTP GET.
      ]

      component productCmstAdaptorMediator [
          Product CMST Adaptor Mediator

          Adapts product master data to flat reference file of key (product code) to values (gtins, product attributes, etc).
          Does this on a set schedule, storing the single reference file for retrieval of the CMST system via HTTP GET.
      ]

      component gtinQueryCmstAdaptorMediator #limegreen [
          GTIN Query CMST Adapter Mediator

          Adapts a flat GTIN reference file, to a GTIN lookup.
      ]

      component productQueryCmstAdaptorMediator #limegreen [
          Product Query CMST Adaptor Mediator

          Adapts a flat Product reference file, to a Product lookup. 
      ]

      component marketAuthorizationCmstAdaptorMediator #limegreen [
          Market Authorization CMST Adaptor Mediator

          Secures and adapts the Market Authorization data, using a query interface.
      ]
    }

    [gtinQueryCmstAdaptorMediator] ---> [gtinCmstAdaptorMediator]: Cached GTIN reference
    [productQueryCmstAdaptorMediator] ---> [productCmstAdaptorMediator]: Cached Product reference

    [gtinCmstAdaptorMediator] --> [productMediator] : GET Item : Bundle<Item>
    [productCmstAdaptorMediator] --> [productMediator] : GET Product: Bundle<Product>
    
    [marketAuthorizationCmstAdaptorMediator] ---> [gtinCmstAdaptorMediator]: Cached MA by GTIN
    [marketAuthorizationCmstAdaptorMediator] ---> [productCmstAdaptorMediator]: Cached MAs by Product Id

    [productMediator] --> [pcmtFhirApi] : GET Product or Bundle : Bundle<Product|Item>
}

frame "Clients" {
    component gtinLookup [
        GTIN Lookup (CMST)

        Take barcode and lookup Item via decoded GTIN
    ]

    component productLookup [
        Product Lookup (CMST)

        Take a Malawi Product identifier, and return
        possible Item information (e.g. GTIN, etc)    
    ]

    [gtinLookup] -----> [gtinCmstAdaptorMediator] : GET flat reference file
    [productLookup] -----> [productCmstAdaptorMediator] : GET flat reference file
    [gtinLookup] --> [marketAuthorizationCmstAdaptorMediator]: GET MA
    [productLookup] --> [marketAuthorizationCmstAdaptorMediator]: GET MA
    [gtinLookup] --> [gtinQueryCmstAdaptorMediator]: GET by a gtin
    [productLookup] --> [productQueryCmstAdaptorMediator]: GET by a Product ID
    
    '[marketAuthorizationCmstAdaptorMediator] --[hidden]-- [productQueryCmstAdaptorMediator]
}
@enduml
```
